/// Page de communication avec un distributeur.
///
/// Cette page prend un distributeur en paramètre, s'y connecte et
/// permet à l'utilisateur de communiquer avec lisant des valeurs du
/// distributeur, en lui envoyant des valeurs et en recevant des
/// valeurs par notification.
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_ble_lib/flutter_ble_lib.dart';
import 'package:meta/meta.dart';

import 'bluetooth.dart';

/// Page pour communiquer avec le distributeur.
class PageDistributeur extends StatefulWidget {
  /// Périphérique Bluetooth du distributeur.
  final Peripheral distributeur;

  PageDistributeur({Key key, @required this.distributeur})
      : assert(distributeur != null),
        super(key: key);

  @override
  _PageDistributeurEtat createState() => _PageDistributeurEtat();
}

class _PageDistributeurEtat extends State<PageDistributeur> {
  /// Si la liste de services du distributeur a été découverte.
  bool servicesDecouverts = false;

  @override
  void initState() {
    super.initState();
    _connecterPeriph();
  }

  /// Effectue la connexion au périphérique
  /// et démarre la recherche de ses services
  void _connecterPeriph() async {
    if (await widget.distributeur.isConnected()) {
      _decouvrirServicesEtCars();
    } else {
      try {
        await widget.distributeur.connect();
        _decouvrirServicesEtCars();
      } catch (_) {
        Navigator.pop(
            context, "La connexion au périphérique n'a pas pue être établie");
      }
    }

    widget.distributeur
        .observeConnectionState(completeOnDisconnect: true)
        .listen((etatPeriph) {
      if (etatPeriph == PeripheralConnectionState.disconnecting ||
          etatPeriph == PeripheralConnectionState.disconnected) {
        Navigator.pop(context, "La connexion au périphérique a été perdue");
      } else {
        if (etatPeriph == PeripheralConnectionState.connected) {
          _decouvrirServicesEtCars();
        }
      }
    });
  }

  /// Découvre la liste des services et des caractéristiques
  /// du périphérique connecté.
  void _decouvrirServicesEtCars() async {
    widget.distributeur.discoverAllServicesAndCharacteristics().then((_) {
      setState(() => servicesDecouverts = true);
    }).catchError((e) {
      Navigator.pop(
          context, "Impossible d'obtenir les caractéristiques du distributeur");
    });
  }

  /// Retour un indicateur de progrès signalant que
  /// la connexion au distributeur est en cours.
  Widget _indicateurProgres() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircularProgressIndicator(),
          SizedBox(height: 16.0),
          Text("En cours de connexion ..."),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.distributeur.name != null
            ? "${widget.distributeur.name} (${widget.distributeur.identifier})"
            : widget.distributeur.identifier),
      ),
      body: servicesDecouverts
          ? _FormComm(distributeur: widget.distributeur)
          : _indicateurProgres(),
    );
  }

  @override
  void dispose() {
    widget.distributeur.disconnectOrCancelConnection();
    super.dispose();
  }
}

/// Formulaire pour envoyer et recevoir des messages
/// par Bluetooth au distributeur.
class _FormComm extends StatefulWidget {
  /// Périphérique Bluetooth du distributeur.
  final Peripheral distributeur;

  _FormComm({Key key, @required this.distributeur})
      : assert(distributeur != null),
        super(key: key);

  @override
  _FormCommEtat createState() => _FormCommEtat();
}

class _FormCommEtat extends State<_FormComm> {
  /// Contrôleur du champs de texte du message reçu du distributeur.
  /// Ceci permet d'afficher le message reçu du distributeur.
  final TextEditingController msgRecu = TextEditingController();

  /// Contrôleur du champs de texte du message à envoyer.
  /// Ceci permet à l'utilisateur de choisir le texte à envoyer.
  final TextEditingController msgEnvoyer = TextEditingController();

  /// Démarre l'écoute de notifications.
  @override
  void initState() {
    super.initState();

    widget.distributeur
        .monitorCharacteristic(ID_SERVICE_PERSO, ID_CARACTERISTIQUE_NOTIF)
        .listen((notif) {
      String text = String.fromCharCodes(notif.value);
      Scaffold.of(context).showSnackBar(
        SnackBar(content: Text(text)),
      );
    }, onError: (e) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
            content: Text(
                "Il est impossible de recevoir des notifications du distributeur")),
      );
    });
  }

  /// Lit un message du distributeur par Bluetooth et l'affiche
  /// dans le champs de message reçu.
  void _lire() async {
    try {
      CharacteristicWithValue car = await widget.distributeur
          .readCharacteristic(ID_SERVICE_PERSO, ID_CARACTERISTIQUE_LIRE);
      String text = String.fromCharCodes(car.value);
      setState(() {
        msgRecu.text = text;
      });
    } catch (e) {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(e.toString())));
    }
  }

  /// Envoi un message par Bluetooth au distributeur.
  void _envoyer() async {
    String msg;
    try {
      final valeur = Uint8List.fromList(msgEnvoyer.text.codeUnits);
      await widget.distributeur.writeCharacteristic(
          ID_SERVICE_PERSO, ID_CARACTERISTIQUE_ECRIRE, valeur, false);
      msg = "Le message a été envoyée avec succès";
    } catch (_) {
      msg = "Le message n'a pas pu être envoyé";
    }
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(msg)));
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(8.0),
      children: [
        Text(
          "Message reçu: ",
          style: Theme.of(context).textTheme.headline6,
        ),
        TextField(controller: msgRecu, readOnly: true),
        Align(
          alignment: Alignment.topRight,
          child: MaterialButton(
            child: Text("Lire"),
            color: Colors.grey[300],
            onPressed: () => _lire(),
          ),
        ),
        SizedBox(height: 32.0),
        Text(
          "Message à envoyer: ",
          style: Theme.of(context).textTheme.headline6,
        ),
        TextField(controller: msgEnvoyer),
        Align(
          alignment: Alignment.topRight,
          child: MaterialButton(
            child: Text("Envoyer"),
            color: Colors.grey[300],
            onPressed: () {
              _envoyer();
            },
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    msgRecu.dispose();
    msgEnvoyer.dispose();
    super.dispose();
  }
}
