/// ID du service personnalisé du distributeur
const ID_SERVICE_PERSO = "18944ab4-54a3-472a-871f-9b4dc69c5741";

/// ID de la caractéristique du distributeur pour lire une valeur.
const ID_CARACTERISTIQUE_LIRE = "0b450138-b45a-47ad-b3ef-ee16a4323007";

/// ID de la caractéristique du distributeur pour lire écrire une valeur.
const ID_CARACTERISTIQUE_ECRIRE = "5f9b7668-d164-4d06-9163-4b189279d56e";

/// ID de la caractéristique du distributeur pour recevoir une notification.
const ID_CARACTERISTIQUE_NOTIF = "7485df96-8867-41b6-8d85-c77cdc95e4bf";
