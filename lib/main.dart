/// Programme principal de l'application.
///
/// Démarre l'application en affichant [PageAccueil].
import 'package:flutter/material.dart';

import 'accueil.dart';

void main() {
  runApp(MaterialApp(
    title: 'Medicator',
    theme: ThemeData(
      primarySwatch: Colors.blue,
      visualDensity: VisualDensity.adaptivePlatformDensity,
    ),
    home: PageAccueil(),
  ));
}
