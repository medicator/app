/// Page d'accueil de l'application
///
/// Cette page cherche tous les distributeurs bluetooth et les affiche
/// dans une liste. Lorsque l'utilisateur appui sur un des éléments de la liste,
/// le distributeur est passé à [PageDistributeur].
import 'package:flutter/material.dart';
import 'package:flutter_ble_lib/flutter_ble_lib.dart';
import 'package:medicator/distributeur.dart';
import 'package:permission_handler/permission_handler.dart';

import 'bluetooth.dart';

/// Page d'accueil de l'application
class PageAccueil extends StatelessWidget {
  PageAccueil({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Connexion à un distributeur"),
        ),
        body: _ContenuAccueil());
  }
}

/// Contenu de la page d'accueil.
/// Ceci affiche l'état du Bluetooth et la liste de périphériques.
class _ContenuAccueil extends StatefulWidget {
  _ContenuAccueil({Key key}) : super(key: key);

  @override
  _ContenuAccueilEtat createState() => _ContenuAccueilEtat();
}

class _ContenuAccueilEtat extends State<_ContenuAccueil> {
  /// Permet de gérer une connexion Bluetooth.
  BleManager bleManager = BleManager();

  /// État de la connexion Bluetooth.
  BluetoothState etatBluetooth;

  /// Si l'app peut faire un scan des appareils Bluetooth.
  PermissionStatus permissionScanBluetooth;

  /// Liste de distributeurs de médicaments.
  List<Peripheral> peripheriquesBluetooth = [];

  /// Texte à afficher pour les états du module Bluetooth.
  static const ETATS_BLUETOOTH = <String>[
    "Inconnu", // BluetoothState.UNKNOWN
    "Non supporté", // BluetoothState.UNSUPPORTED
    "Non autorisé", // BluetoothState.UNAUTHORIZED
    "Allumé", // BluetoothState.POWERED_ON
    "Éteint", // BluetoothState.POWERED_OFF
    "Réinitialisation", // BluetoothState.RESETTING
  ];

  @override
  void initState() {
    super.initState();
    _init();
  }

  /// Initialise le client Bluetooth, allume le Bluetooth
  /// et débute la recherche de périphériques.
  void _init() async {
    // Initialiser la connexion au Bluetooth.
    bleManager.createClient();

    BluetoothState etatBt = await bleManager.bluetoothState();
    if (etatBt == BluetoothState.POWERED_OFF) {
      // Démarrer le Bluetooth.
      await bleManager.enableRadio();
      etatBt = await bleManager.bluetoothState();
      bleManager.enableRadio();
    }

    if (etatBt == BluetoothState.POWERED_ON) {
      _demarrerScan();
    }

    bleManager.observeBluetoothState().listen((etatBt) {
      if (etatBt != BluetoothState.POWERED_ON) {
        bleManager.stopPeripheralScan();
      } else {
        _demarrerScan();
      }

      setState(() {
        etatBluetooth = etatBt;
        if (etatBt != BluetoothState.POWERED_ON) {
          peripheriquesBluetooth.clear();
        }
      });
    });

    setState(() {
      etatBluetooth = etatBt;
    });
  }

  /// Démarre et gère le scan de périphériques
  void _demarrerScan() async {
    // Essayer d'obtenir la permission d'effectuer un scan.
    PermissionStatus statut = await Permission.locationWhenInUse.request();

    final periphConnectes =
        await bleManager.connectedPeripherals(const [ID_SERVICE_PERSO]);

    setState(() {
      peripheriquesBluetooth = periphConnectes;
      permissionScanBluetooth = statut;
    });

    if (statut == PermissionStatus.granted) {
      bleManager.startPeripheralScan(uuids: const [ID_SERVICE_PERSO]).listen(
          (ScanResult resultat) {
        if (resultat.advertisementData.serviceUuids != null) {
          Peripheral p = peripheriquesBluetooth.firstWhere(
              (p) => p.identifier == resultat.peripheral.identifier,
              orElse: () => null);
          if (p == null) {
            setState(() {
              peripheriquesBluetooth.add(resultat.peripheral);
            });
          } else if (p.name == null && resultat.peripheral.name != null) {
            setState(() {
              p.name = resultat.peripheral.name;
            });
          }
        }
      });
    }
  }

  /// Ouvre une [PageDistributeur] pour le périphérique donné et
  /// affiche le message d'erreur qu'elle retourne.
  void _ouvrirPeriph(Peripheral p) async {
    String message = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => PageDistributeur(distributeur: p),
      ),
    );
    if (message != null) {
      Scaffold.of(context).showSnackBar(
        SnackBar(content: Text(message)),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(children: [
        Row(children: [
          Text(
            "État de la connexion Bluetooth: ",
            style: Theme.of(context).textTheme.subtitle1,
          ),
          Text(
            etatBluetooth != null ? ETATS_BLUETOOTH[etatBluetooth.index] : "?",
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ]),
        SizedBox(height: 8.0),
        if (permissionScanBluetooth != null &&
            permissionScanBluetooth != PermissionStatus.granted) ...[
          Text(
              "L'application n'a pas la permission de découvrir des appareils Bluetooth",
              style: Theme.of(context)
                  .textTheme
                  .subtitle1
                  .copyWith(color: Colors.red)),
          SizedBox(height: 8.0),
        ],
        ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: peripheriquesBluetooth.length,
          itemBuilder: (context, i) {
            Peripheral p = peripheriquesBluetooth[i];
            return Card(
              child: ListTile(
                title: Text(p.name != null
                    ? "${p.name} (${p.identifier})"
                    : p.identifier),
                onTap: () => _ouvrirPeriph(p),
              ),
            );
          },
        ),
        MaterialButton(
          child: Text("Rafraîchir"),
          onPressed: () {
            _demarrerScan();
          },
          color: Colors.grey[200],
        ),
      ]),
    );
  }

  @override
  void dispose() {
    bleManager.destroyClient();
    super.dispose();
  }
}
